import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodoItemsComponent } from './todo-items/todo-items.component';
import {ManualComponent} from './manual/manual.component';
import {TodoItemDetailsComponent} from './todo-item-details/todo-item-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/todos', pathMatch: 'full' },
  { path: 'todos', component: TodoItemsComponent },
  { path: 'manual', component: ManualComponent},
  { path: 'todo/:id', component: TodoItemDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
