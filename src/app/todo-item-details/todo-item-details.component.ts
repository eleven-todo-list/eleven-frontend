import { Component, OnInit } from '@angular/core';
import {TodoItemService} from '../todo-item.service';
import {TodoItem} from '../todo-item';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-todo-item-details',
  templateUrl: './todo-item-details.component.html',
  styleUrls: ['./todo-item-details.component.css']
})
export class TodoItemDetailsComponent implements OnInit {
  public todoItem: TodoItem;

  constructor(private todoItemService: TodoItemService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.getTodoItem();
  }

  getTodoItem(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.todoItemService.getTodoItem(id)
      .subscribe(todo => this.todoItem = todo);
  }

  updateTodoItem(): void {
    this.todoItemService.updateTodoItem(this.todoItem)
      .subscribe(response => {
        this.router.navigate(['/todos']);
      });
  }

}
