import { Injectable } from '@angular/core';
import { TodoItem } from './todo-item';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TodoItemService {
  private todoItemsUrl = '/api/v1/todoitems';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getTodoItems(): Observable<TodoItem[]> {
    return this.http.get<TodoItem[]>(this.todoItemsUrl);
  }

  getTodoItem(id: number): Observable<TodoItem> {
    const url = `${this.todoItemsUrl}/${id}`;
    return this.http.get<TodoItem>(url);
  }

  updateTodoItem(todoItem: TodoItem): Observable<any> {
    const url = `${this.todoItemsUrl}/${todoItem.id}`;
    return this.http.put(url, todoItem, this.httpOptions);
  }

  createTodoItem(todoItem: TodoItem): Observable<TodoItem> {
    return this.http.post<TodoItem>(this.todoItemsUrl, todoItem, this.httpOptions);
  }

  deleteTodoItem(id: number): Observable<any> {
    const url = `${this.todoItemsUrl}/${id}`;
    console.log('delete, delete');
    return this.http.delete(url);
  }
}
