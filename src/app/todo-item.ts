export class TodoItem {
  id: number;
  name: string;
  completed: boolean;
  dueDate: any;
}
