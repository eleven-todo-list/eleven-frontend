import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TodoItem} from '../todo-item';
import { TodoItemService } from '../todo-item.service';

@Component({
  selector: 'app-todo-items',
  templateUrl: './todo-items.component.html',
  styleUrls: ['./todo-items.component.css']
})
export class TodoItemsComponent implements OnInit {

  displayedColumns: string[] = ['check', 'name', 'dueDate', 'buttons'];
  dataSource: MatTableDataSource<TodoItem>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private todoItemService: TodoItemService) {}

  ngOnInit() {
    this.getTodoItems();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getTodoItems(): void {
     this.todoItemService.getTodoItems()
       .subscribe(todoItems => {
         this.dataSource = new MatTableDataSource(todoItems);
         this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
       });
  }

  createTodoItem(): void {
    const todoItem = new TodoItem();
    todoItem.name = 'New todo item';
    todoItem.completed = null;
    this.todoItemService.createTodoItem(todoItem)
       .subscribe(response => this.getTodoItems());
  }

  deleteTodoItem(id: number) {
    this.todoItemService.deleteTodoItem(id)
      .subscribe(response => this.getTodoItems());
  }

  updateTodoItem(item: TodoItem) {
    this.todoItemService.updateTodoItem(item)
      .subscribe(response => this.getTodoItems());
  }

}
